# 3D Patch Prediction Fusion
Our 3D Patch Prediction Fusion method

### What is this repository for? ###

* This is the Python code for our 3D Patch Prediction Fusion method used in:
	* IEEE Access paper (2018)
		* https://ieeexplore.ieee.org/document/8573779
		* https://arxiv.org/abs/1803.11078

### Dependencies ###

* Tensorflow (CPU or GPU - GPU is 20 times faster) - https://www.tensorflow.org
* Scipy - https://www.scipy.org/
* Python 2.7 - https://www.python.org/download/releases/2.7/

### How to run ###

Run this command after importing the "3D_Patch_Prediction_Fusion.py" into your program:

	predict_img_with_smooth_windowing(input_image, patch_size, subdivisions, number_of_classes, prediction_function)
	
* Where "input_image" is the array of the original size image you want to predict
* "patch-size" is the window size of your designated model patches, 
* "subdivisions" is the amount of intersection between patches (minimum of 2, should be an even number) with formulation of:   patch_size * (1 - 1/subdivisions)
	* 2 means 50%:  patch_size * (1 - 1/2) = 1/2 * patch_size
* "number_of_classes" is the number of classes to be predicted
* "prediction_function" is the predict function of your trained model.

### Contact ###

* Raein Hashemi (hashemi.s@husky.neu.edu)
* Ali Gholipour (Ali.Gholipour@childrens.harvard.edu)
